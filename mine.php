<?php

echo("Coordonnées du robot en X :" . "\n");
$robotX = trim(fgets(STDIN));

echo("Coordonnées du robot en Y :" . "\n");
$robotY = trim(fgets(STDIN));

echo("Coordonnées de la mine en X :" . "\n");
$mineX = trim(fgets(STDIN));

echo("Coordonnées de la mine en Y :" . "\n");
$mineY = trim(fgets(STDIN));

chemin($robotX, $robotY, $mineX, $mineY);

function directionRobot($robotX, $robotY, $mineX, $mineY){
    $eopos = "";
    $nspos = "";
    
    if($robotX < $mineX){
        $eopos = "E";
    } else {
        $eopos = "O";
    }

    if($robotY < $mineY){
        $nspos = "S";
    } else {
        $nspos = "N";
    }

    return $nspos . $eopos;
}

// echo(directionRobot($robotX, $robotY, $mineX, $mineY));

function chemin($posRobotX, $posRobotY, $posMineX, $posMineY){
    while (true){
        $position = directionRobot($posRobotX, $posRobotY, $posMineX, $posMineY);

        echo("Le robot se situe : " . ($position == "" ? "sur la mine" : $position) . " (rx:" . $posRobotX . ", ry:" . $posRobotY . ", mx:" . $posMineX . ", my:" . $posMineY . ")\n");

        if($position == ""){
            echo("Sur la mine !!!");
            return;
        }

        $moving = "";

        if(strpos($position, 'N') !== false){
            $posRobotY ++;
            $moving .= "N";
        }

        if(strpos($position, 'S') !== false){
            $posRobotY --;
            $moving .= "S";
        }

        if(strpos($position, 'E') !== false){
            $posRobotX ++;
            $moving .= "E";
        }

        if(strpos($position, 'O') !== false){
            $posRobotY --;
            $moving .= "O";
        }

        echo ("Le robot a bougé en " . $moving . " !");
    }
}

